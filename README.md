This is a special chess project to introduce inheritance anf work on list + loop management.

Instructions:
    1 - Initiate a board of X*X
    2 - Create a piece class who's the base class for future type of game piece
        take care of the owner and the orientation
    3 - Create a move function equal to the "pion" game piece
        the function change depends of the side of the chess
    4 - Create all piece for the chess with their own move function
    5 - Initialize the board with all game piece
